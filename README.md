= hugo-gradle-plugin

A plugin to wrap simplify website publishing with [Hugo](https://gohugo.io/) simple using Gradle is a very gradlesque way. Will also deal with downloading Hugo on supported platforms.

== Objectives

The objectives of the Hugo plugin includes

* Download appropriate version of Hugo and cache in `${gradleUserHomerDir}/native-binaries/hugo`
* Read source from `src/docs/hugo`
* Create dev work to `build/docs/hugo`
* Publish production to specified directory
* Compatibility with `grgit`.

*Work on this has not started yet*: If you would like to contribute or be mentored in writing what will be an awesome plugin and contribution to the Gradle community please get in touch.

P.S. This has been inspired by this [article from Andy Thornton](https://github.com/opensourceway/asciidoc-blog) 